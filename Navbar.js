import React from "react";

export default function Navbar(){
    return (
       <nav>
             <img src="../public/logo192.png" />
             <h3>ReactFacts</h3>
             <h4>React Course -Project 1</h4>
       </nav>
    )
}